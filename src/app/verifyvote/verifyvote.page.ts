import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NavController, AlertController, ToastController, LoadingController } from '@ionic/angular';
import Utils from '../common/utils/Utils';

@Component({
  selector: 'app-verifyvote',
  templateUrl: 'verifyvote.page.html',
  styleUrls: ['verifyvote.page.scss'],
})
export class VerifyvotePage {
  
  electionResponse: Observable<any>;
  allElections = [];
  selectedElection: any;
  voterId: any;
  dob: any;
  transactionId: any;
  candidate: any;

  constructor(public navCtrl: NavController, public httpClient: HttpClient, private alertController: AlertController, private loadingCtrl: LoadingController) { 
    let loading = Utils.presentLoading(this.loadingCtrl);
      
    this.electionResponse = this.httpClient.get(Utils.GET_SERVER_URL + '/elections');
    this.electionResponse
    .subscribe(data => {
      console.log('election data: ', data);
      this.allElections = data;
      loading.then((loader)=> {
        loader.dismiss();
      });
    });
    this.candidate = undefined;
  }

  getVote() {
    console.log("get vote");
    console.log("election",this.selectedElection);
    console.log("voterId",this.voterId);
    console.log("dob",this.dob);
    console.log("transactionId",this.transactionId);
    if (this.validInput()) {
      let loading = Utils.presentLoading(this.loadingCtrl);
      this.httpClient.post(Utils.GET_SERVER_URL + '/verifyvote', {
        electionId: this.selectedElection.id,
        voterId: this.voterId,
        dob: this.dob,
        transactionId: this.transactionId
      }).subscribe((response) => {
        response = JSON.parse((response.toString()));
        console.log(response);
        Utils.showDismissAlert(this.alertController, "", response['message']);
        if (response['candidateName']) {
          this.candidate = {
            name: response['candidateName'],
            photoUrl: response['candidatePhotoUrl']
          }
        } else {
          this.candidate = undefined;
        }
        loading.then((loader)=> {
          loader.dismiss();
        });
      });
    }
  }

  validInput() {
    let isValid = true;
    if (this.selectedElection === undefined) {
      Utils.showDismissAlert(this.alertController, "Error", "Please select an election");
      isValid = false;
    } else if (this.voterId === undefined) {
      Utils.showDismissAlert(this.alertController, "Error", "Please enter your voter id");
      isValid = false;
    } else if (this.dob === undefined) {
      Utils.showDismissAlert(this.alertController, "Error", "Please select your date of birth");
      isValid = false;
    } else if (this.transactionId === undefined) {
      Utils.showDismissAlert(this.alertController, "Error", "Please enter your transaction id");
      isValid = false;
    }

    return isValid;
  }

}
