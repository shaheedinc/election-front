import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyvotePage } from './verifyvote.page';

describe('VerifyvotePage', () => {
  let component: VerifyvotePage;
  let fixture: ComponentFixture<VerifyvotePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyvotePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyvotePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
