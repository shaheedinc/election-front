import { AlertController, LoadingController } from '@ionic/angular';

export default class Utils {

    static GET_SERVER_URL = "http://142.3.83.4:4567";

    static async showDismissAlert(alertController: AlertController, title: String, message: String) {
        const alert = await alertController.create({
          header: title.toString(),
          message: message.toString(),
          buttons: ['Dismiss']
        });
        await alert.present();
    }

    static getPostHeaders() {
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        return headers;
    }

    static async presentLoading(loadingController: LoadingController) {
        const loading = await loadingController.create({
            message: 'Please wait...'
        });
        await loading.present();
        return loading;
    }

    static async hideLoading(loader: any) {
        loader.dismiss();
      }
    
}