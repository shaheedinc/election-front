import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NavController, AlertController, ToastController, LoadingController } from '@ionic/angular';
import Election from '../common/model/Election';
import Utils from '../common/utils/Utils';
import * as Clipboard from 'clipboard/dist/clipboard.min.js';

@Component({
  selector: 'app-result',
  templateUrl: 'result.page.html',
  styleUrls: ['result.page.scss'],
})
export class ResultPage {
  
  electionResponse: Observable<any>;
  allElections = [];
  selectedElection: any;
  gotResult = false;
  result: [];

  constructor(public navCtrl: NavController, public httpClient: HttpClient, private alertController: AlertController, private loadingCtrl: LoadingController) { 
    let loading = Utils.presentLoading(this.loadingCtrl);
      
    this.electionResponse = this.httpClient.get(Utils.GET_SERVER_URL + '/elections');
    this.electionResponse
    .subscribe(data => {
      console.log('election data: ', data);
      this.allElections = data;
      loading.then((loader)=> {
        loader.dismiss();
      });
    });
  }

  getElectionResult() {
    console.log("getElectionResult");
    if (this.validInput()) {
      let loading = Utils.presentLoading(this.loadingCtrl);
      this.httpClient.post(Utils.GET_SERVER_URL + '/result', {
        electionId: this.selectedElection.id
      }).subscribe((response) => {
        response = JSON.parse((response.toString()));
        console.log(response['candidates']);
        if (response['message']) {
          Utils.showDismissAlert(this.alertController, "", response['message']);
          this.gotResult = false;
          this.result = undefined;
        } else {
          this.gotResult = true;
          this.result = JSON.parse(response['candidates']);
          console.log(this.result);
        }
        loading.then((loader)=> {
          loader.dismiss();
        });
      });
    }
  }

  validInput() {
    let isValid = true;
    if (this.selectedElection === undefined) {
      Utils.showDismissAlert(this.alertController, "Error", "Please select an election");
      isValid = false;
    }

    return isValid;
  }

}
