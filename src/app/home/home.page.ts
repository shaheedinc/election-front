import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import Election from '../common/model/Election';
import Utils from '../common/utils/Utils';
import * as Clipboard from 'clipboard/dist/clipboard.min.js';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  
  electionResponse: Observable<any>;
  allElections = [];
  selectedElection: any;
  voterId: any;
  dob: any;
  selectedCandidate: any;
  clipboard: any;
  txtToCopy: any;

  constructor(public navCtrl: NavController, public httpClient: HttpClient, private alertController: AlertController, private loadingCtrl: LoadingController) { 
    let loading = Utils.presentLoading(this.loadingCtrl);
    this.electionResponse = this.httpClient.get(Utils.GET_SERVER_URL + '/elections');
    this.electionResponse
    .subscribe(data => {
      console.log('election data: ', data);
      this.allElections = data;
      loading.then((loader)=> {
        loader.dismiss();
      });
    });
    this.clipboard = new Clipboard('#cpyBtn');
    this.clipboard.on('success', () => Utils.showDismissAlert(this.alertController, "", "Copied to your clipboard"));
    this.txtToCopy = undefined;
  }

  castVote() {
    console.log("cast vote");
    console.log("election",this.selectedElection);
    console.log("candidate",this.selectedCandidate);
    console.log("voterId",this.voterId);
    console.log("dob",this.dob);
    if (this.validInput()) {
      let loading = Utils.presentLoading(this.loadingCtrl);
      this.httpClient.get(Utils.GET_SERVER_URL + "/server?userId=" + this.voterId)
      .subscribe((response) => {
        let res = JSON.parse(response.toString());
        console.log(res);
        this.httpClient.post(Utils.GET_SERVER_URL + '/vote', {
          electionId: this.selectedElection.id,
          candidateId: this.selectedCandidate,
          voterId: this.voterId,
          dob: this.dob
        })
        .subscribe((response) => {
          response = JSON.parse((response.toString()));
          console.log(response);
          Utils.showDismissAlert(this.alertController, "", response['message']);
          if (response['transactionId']) {
            this.txtToCopy = response['transactionId'];
          }
          loading.then((loader)=> {
            loader.dismiss();
          });
        });
      });
    }
  }

  validInput() {
    let isValid = true;
    if (this.selectedElection === undefined) {
      Utils.showDismissAlert(this.alertController, "Error", "Please select an election");
      isValid = false;
    } else if (this.voterId === undefined) {
      Utils.showDismissAlert(this.alertController, "Error", "Please enter your voter id");
      isValid = false;
    } else if (this.dob === undefined) {
      Utils.showDismissAlert(this.alertController, "Error", "Please select your date of birth");
      isValid = false;
    } else if (this.selectedCandidate === undefined) {
      Utils.showDismissAlert(this.alertController, "Error", "Please select a candidate");
      isValid = false;
    }

    return isValid;
  }

}
